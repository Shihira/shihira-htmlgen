shihira-htmlgen
===============

Shihira-htmlgen allows you to edit easily your article;
then generate and upload the final html based on your
templates according to settings.xml file.

HOW-TO-USE
---------------

**First of all, modify settings.xml. (refer to the settings section)**

Now see the shihira-htmlgen main window:

* **Title**: A title displays in browsers' caption, shows in the range marked out with:
  `<!-- shihira-htmlgen-BEGIN: title --><!-- shihira-htmlgen-END: title -->`
  and lists on the list page.
* **Time**: Shihira-htmlgen uses time sign to mark **HTMLs' filename** and the
  **Illustrations' filename**. The time displays in `<!-- shihira-htmlgen-BEGIN:
  time --><!-- shihira-htmlgen-END: time -->`.
* **Content**: Supports HTML and recommends to use HTML. Provides preview in **Read Effect**.
  Contents display in `<!-- shihira-htmlgen-BEGIN: content --><!-- shihira-htmlgen-END: content -->`
* **Image Path**: For each article, only one illustration is allowed. Displays in
  `<!-- shihira-htmlgen-BEGIN: image --><!-- shihira-htmlgen-END: image -->`

Having finished all textboxs, __CLICK UPLOAD BOTTON__.

Settings
---------------

The config/setting.xml is actually very easily understood, except the last few items.

* **comment_regex**: It matchs the shihira-htmlgen specified HTML comment, required that
  the 1st and the 2st group are name and content.
* **list_format**: Both image\_format and list\_format are in line with C/C++ string format
  (i.e. printf, scanf uses). list format are required less than *2 args*:

    1. url to the article;
    2. title of the article; 


* **image_format**: image\_format are required less than *1 arg*:

    1. url to the image;

Template
----------------

Template files include a article template and a list template, which path provided by
settings.xml (refer to raw file). You can modify your template freely, but don't forget:

* to keep the following comment named _(in article(A) or list(L)_ template):

    1. page\_title      in A
    2. title            in A & L
    3. time             in A
    4. image            in A
    5. content          in A
    6. list             in     L

* not to put page\_title the comment inside a `<title></title>`

Compiling
----------------

Compile Using CMake. Do these on unix:
`
cd shihira-htmlgen
cd build
cmake .
make
cd ../config
mkdir temporary
vi settings.xml
cd ..
./shihira-htmlgen
`

**DEPENDENCIES**:

* [Qt4](http://qt-project.org/).QtGui & QtCore
* [libcurl](http://curl.haxx.se/libcurl/)
* [Boost](http://www.boost.org/).Regex
* [TinyXML](http://sourceforge.net/projects/tinyxml/)

