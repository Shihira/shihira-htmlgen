#include "main_ui.h"

int main(int argc, char* argv[])
{
        QApplication app(argc, argv);

        main_ui ui;
        ui.show();

        QDir().mkpath(app.applicationDirPath() + "/config/temporary");

        return app.exec();
}

