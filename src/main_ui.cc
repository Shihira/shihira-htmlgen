#include "main_ui.h"
#include "main_ui.moc"

#include "comment_tag.h"
#include "sinc/string_ext.h"

class upload_thread : public QThread {
public:
        upload_thread(main_ui* parent) : QThread(parent) { }
        void run() { ((main_ui*)parent())->upload_for_thread(); }
};


main_ui::main_ui(QWidget* parent)
        : QWidget(parent)
{
        setupUi(this);
        m_settings.load_file(QApplication::applicationDirPath().toStdString() + "/config/settings.xml");

        //set widgets' value with settings value
        check_time->setCheckState(m_settings.sync_system_time? Qt::Checked : Qt::Unchecked);
        check_verbose->setCheckState(!m_settings.verbose.empty()? Qt::Checked : Qt::Unchecked);

        net_article_file.timeout = m_settings.upload_timeout_ms;
        net_list_file.timeout = m_settings.upload_timeout_ms;

        QTextCodec::setCodecForCStrings(QTextCodec::codecForName(m_settings.charset.c_str()));

        connect(tab,            SIGNAL(currentChanged(int)),
                this,           SLOT(apply_html(int)));
        connect(button_explore, SIGNAL(clicked()),
                this,           SLOT(show_open()));
        connect(check_time,     SIGNAL(clicked()),
                this,           SLOT(set_edit_time_ena()));
        connect(button_upload,  SIGNAL(clicked()),
                this,           SLOT(upload()));
        connect(this,           SIGNAL(progress(int)),
                progress_upload,SLOT(setValue(int)));
        connect(this,           SIGNAL(upload_status(int)),
                this,           SLOT(set_status(int)));

        set_edit_time_ena();
}

////SHORT SLOTS
void main_ui::apply_html(int tab_index)
{
        if(tab_index == 1) // the second tab
                edit_real->setHtml(edit_content->toPlainText());
}
void main_ui::show_open() { edit_path->setText(QFileDialog::getOpenFileName(this, "Open Image",
                            edit_path->text(), "Image Files (*.png *.jpg *.gif)")); }
void main_ui::set_edit_time_ena() { edit_time->setEnabled(!bool(check_time->checkState()));
                                    set_edit_time(); }
void main_ui::set_edit_time()
{
        if(!edit_time->isEnabled()) { // when checked, disable the edit and auto update it
                edit_time->setText(QDate::currentDate().toString("yyyy-MM-dd-")
                                 + QTime::currentTime().toString("hh-mm-ss"));
                QTimer::singleShot(1000, this, SLOT(set_edit_time()));
        }
}

void main_ui::set_status(int code)
{
        if(!code) progress_upload->setFormat("%p%");
        else progress_upload->setFormat(QString("Failed: ") +
                        QString(curl_easy_strerror((CURLcode)code)));
}

////UPLOAD
#define output(x) std::cout << #x << ":\t" << x << "\n";
#define verify_upload(x) if(x) { upload_status(x); output(x); return; }

void main_ui::upload()
{
        upload_thread * thread = new upload_thread(this);
        thread->start();
}
void main_ui::upload_for_thread()
{
        emit upload_status(0);
        emit progress(0); // ------------------------------------------------- 0

        //read data from widgets
        std::string title = edit_title->text().toStdString();
        std::string time = edit_time->text().toStdString();
        std::string filename = time + ".html";
        std::string content = edit_content->toPlainText().toStdString();
        std::string verbose_file = check_verbose->checkState()? m_settings.verbose : "";
        std::string image = QFileInfo(edit_path->text()).exists() ? // determine if the image exists
                    time + "." + QFileInfo(edit_path->text()).suffix().toStdString() : "";

        // all kinds of address
        std::string
        loc_art  = QApplication::applicationDirPath().toStdString() + "/config/temporary/" + filename,
        loc_lst  = QApplication::applicationDirPath().toStdString() + "/config/temporary/" + time + "-" + m_settings.list_file_name,
        loc_img  = edit_path->text().toStdString(),
        ftp_art  = m_settings.ftp_address + m_settings.article_remote_path + filename,
        ftp_lst  = m_settings.ftp_address + m_settings.list_remote_path + m_settings.list_file_name,
        ftp_img  = m_settings.ftp_address + m_settings.image_remote_path + image,
        http_art = m_settings.http_address + m_settings.article_remote_path + filename,
        http_lst = m_settings.http_address + m_settings.list_remote_path + m_settings.list_file_name,
        http_img = image.empty()? "" : m_settings.http_address + m_settings.image_remote_path + image;

        output(loc_art); output(loc_lst); output(loc_img);
        output(ftp_art); output(ftp_lst); output(ftp_img);
        output(http_art); output(http_lst); output(http_img);

        // prepare net tranfer
        net_article_file.local = loc_art;
        net_article_file.remote = ftp_art;
        net_article_file.log = verbose_file;

        net_list_file.local = loc_lst;
        net_list_file.remote = ftp_lst;
        net_list_file.log = verbose_file;

        net_image_file.local = loc_img;
        net_image_file.remote = ftp_img;
        net_list_file.log = verbose_file;

        emit progress(10); // ----------------------------------------------- 10
        verify_upload(net_list_file.download());

        emit progress(25); // ----------------------------------------------- 25
        // generate list file
        comment_tag list_file(m_settings.comment_regex, 1, 2);
        list_file.load_file(loc_lst);
        std::string cur_list;
        string_format(cur_list, m_settings.list_format, http_art.c_str(), title.c_str());
        list_file["list"] = cur_list + list_file["list"];
        list_file.save_file(loc_lst);

        //generate article
        comment_tag article_file(m_settings.comment_regex, 1, 2);
        article_file.load_file(QApplication::applicationDirPath().toStdString() + "/config/template.html");
        std::string image_tag; string_format(image_tag, m_settings.image_format, http_img.c_str());
        article_file["image"] = image_tag;
        article_file["page_title"] = "<title>" + title + "</title>";
        article_file["title"] = title;
        article_file["time"] = time;
        article_file["content"] = content;
        article_file.save_file(loc_art);

        emit progress(40); // ----------------------------------------------- 40
        verify_upload(net_list_file.upload());
        emit progress(60); // ----------------------------------------------- 60
        verify_upload(net_article_file.upload());
        emit progress(80); // ----------------------------------------------- 80
        if(!image.empty()) verify_upload(net_image_file.upload());
        emit progress(100); // ---------------------------------------------- 100
}

