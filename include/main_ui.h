#ifndef  MAIN_UI_INC
#define  MAIN_UI_INC

#include <QtGui>
#include <QMacStyle>
#include "main_ui.uic"

#include "sinc/netfile.h"
#include "sinc/xml_bind.h"

class htmlgen_settings : public xml_bind_file {
public:
        std::string ftp_address;
        std::string http_address;
        std::string list_remote_path;
        std::string article_remote_path;
        std::string image_remote_path;
        std::string list_file_name;
        int upload_timeout_ms;
        int sync_system_time;
        std::string verbose;

        std::string comment_regex;
        std::string list_format;
        std::string image_format;
        std::string charset;

        htmlgen_settings() {
                connect_string("comment_regex", comment_regex);
                connect_string("list_format", list_format);
                connect_string("image_format", image_format);

                connect_string("ftp_address", ftp_address);
                connect_string("http_address", http_address);
                connect_string("list_remote_path", list_remote_path);
                connect_string("article_remote_path", article_remote_path);
                connect_string("image_remote_path", image_remote_path);
                connect_string("list_file_name", list_file_name);
                connect_integer("upload_timeout_ms", upload_timeout_ms);
                connect_integer("sync_system_time", sync_system_time);
                connect_string("verbose", verbose);
                connect_string("charset", charset);
        }
};

class main_ui : public QWidget, protected Ui_main_ui {
        Q_OBJECT

        friend class upload_thread;

        void upload_for_thread();
protected:

        htmlgen_settings m_settings;

        netfile net_image_file;
        netfile net_list_file;
        netfile net_article_file;

public:
        main_ui(QWidget* parent = 0);

protected slots:
        void apply_html(int tab_index);
        void set_edit_time();
        void set_edit_time_ena();
        void set_status(int);

public slots:
        void show_open();
        void upload();

signals:
        void progress(int);
        void upload_status(int);
};

#endif   // ----- #ifndef MAIN_UI_INC  -----
