#ifndef  COMMENT_TAG_INC
#define  COMMENT_TAG_INC

#include <string>
#include <map>
#include <stdio.h>
#include <boost/regex.hpp>
#include <fstream>

class comment_tag {
protected:
        boost::regex m_regex;
        int m_tag_sub;
        int m_content_sub;

        std::map<std::string, std::string> m_content_list; // tag - content
        std::string m_file_content;

        bool foreach_match(boost::smatch& match_results, std::string::const_iterator& iterator) {
                std::string::const_iterator iterator_end = m_file_content.end();
                bool ret = boost::regex_search(iterator, iterator_end, match_results, m_regex);
                iterator = match_results[0].second;
                return ret;
        }

        void generate_list() { // store string that matches regex in a map
                boost::smatch match_results;
                std::string::const_iterator iterator = m_file_content.begin();
                while(foreach_match(match_results, iterator)) {
                        m_content_list[match_results[m_tag_sub].str()] = match_results[m_content_sub];               
                }
        }

        void generate_file(std::string& output) {
                boost::smatch match_results;
                std::string::const_iterator iterator = m_file_content.begin();
                std::string::const_iterator copy_iterator = m_file_content.begin();
                output.erase();
                while(foreach_match(match_results, iterator)) {
                        // document says : m[N].first is the head iterator of
                        //          the matched string, m[N].second is the tail, generally.
                        std::string content = m_content_list[match_results[m_tag_sub].str()];
                        std::string before(copy_iterator, match_results[m_content_sub].first);
                        output += before + content;
                        copy_iterator = match_results[m_content_sub].second;
                }
                output += std::string(copy_iterator, (std::string::const_iterator)m_file_content.end());
        }

public:

        comment_tag(const std::string& regex, int tag_sub, int content_sub)
                : m_regex(regex), m_tag_sub(tag_sub), m_content_sub(content_sub) { }


        void load_file(const std::string& filename) {
                //easily read a file
                FILE* file = fopen(filename.c_str(), "rb");
                if(!file) return;

                fseek(file, 0, SEEK_END);
                size_t file_size = ftell(file);
                rewind(file);

                char* file_content = new char[file_size + 1];
                fread(file_content, sizeof(char), file_size, file);
                file_content[file_size] = 0;

                m_file_content = file_content;

                delete[] file_content;
                fclose(file);
        
                generate_list();
        }

        void save_file(const std::string& filename) {
                //easily write a file
                std::string generated;
                generate_file(generated);

                FILE* file = fopen(filename.c_str(), "wb");
                if(!file) return;
                fwrite(generated.c_str(), sizeof(char),
                       generated.size(), file);
                fclose(file);
        }

        std::string& operator[](const std::string& tag) {
                return m_content_list[tag];
        }
};

#endif   /* ----- #ifndef COMMENT_TAG_INC  ----- */
